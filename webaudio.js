/**
 * Created by Dima on 2015-01-24.
 */
var context;
var testSound;
var testSoundUrl = 'samples/clap.wav';
var sounds;
var bufferLoader;
var tempo = 120;
var eigthNoteTime;
var startTime;
var project;
var bufferObj;

var defaultRythm = [
    {
        'url': 'samples/kick.wav',
        'rhytm' : [
            true,false,true,false,true,true,false,true,true,false,false,false
        ]},
    {
        'url': 'samples/clap.wav',
        'rhytm': [
        false,false,false,false,true,false,false,false,true,false,false,false
    ]},
    {
        'url': 'samples/hihat.wav',
        'rhytm': [
            true,true,true,true,true,true,true,true,true,true,true,true
        ]},
]


window.addEventListener('load', init, false);

function init()
{
    eigthNoteTime = (60 / tempo) / 4;
    context = getAudioContext();
    //loadTestSound(testSoundUrl);
    loadBuffer();
}

function playSound(sound, time)
{
    var source = context.createBufferSource();
    source.buffer = sound;
    source.connect(context.destination);

    if (!source.start)
        source.start = source.noteOn;
    source.start(time);
}

/**
 *
 * @returns {Window.AudioContext}
 */
function getAudioContext()
{
    try {
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        return context = new AudioContext;
    }
    catch (e)
    {
        alert('Web Audio API is not supported in this browser');
    }
}

/**
 *
 * @param url
 */
function loadTestSound(url)
{
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.responseType = 'arraybuffer';

    // Decode asynchronously
    request.onload = function() {
        context.decodeAudioData(request.response, function(buffer) {
            testSound = buffer;
            playSound(testSound, 0);
        }, function (error) {
            window.console.log(error);
        });
    }
    request.send();
}

function loadBuffer()
{
    var sampleList = [];
    for (var i in defaultRythm) {
        var sample = defaultRythm[i];
        sampleList[i] = {
            url: sample.url,
            rhytm: sample.rhytm
        }
    }

    bufferLoader = new BufferLoader(context,
        sampleList,
        function(bufferList) {
            sounds = bufferList;
        });

    bufferLoader.load();
}

/**
 * play simple rythm
 */
function playSimpleRythm()
{
    startTime = context.currentTime;
    var kick = sounds[0].buffer;
    var snare = sounds[2].buffer;
    var hihat = sounds[1].buffer;
    for (var bar = 0; bar < 8; bar++) {
        var time = startTime + bar * 8 * eigthNoteTime;
        // Play the bass (kick) drum on beats 1, 5
        playSound(kick, time);
        playSound(kick, time + 4 * eigthNoteTime);

        // Play the snare drum on beats 3, 7
        playSound(snare, time + 2 * eigthNoteTime);
        playSound(snare, time + 6 * eigthNoteTime);

        // Play the hi-hat every eigth note.
        for (var i = 0; i < 8; ++i) {
            playSound(hihat, time + i * eigthNoteTime);
        }
    }

}

function playRythm()
{
    playOwnRythm(sounds);
}

function playOwnRythm(drumArray)
{
    startTime = context.currentTime;

    for (var i = 0; i < drumArray.length; i++)
    {

        var currentSample = drumArray[i];
        for (var bar = 0; bar < 8; bar++) {
            var time = startTime + bar * 8 * eigthNoteTime;
            for (var j = 0; j < 8;j++)
            {
                if (currentSample.rhytm[j])
                {
                    playSound(currentSample.buffer, time + j * eigthNoteTime);
                }
            }
        }
    }
}